import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, push, get, remove, onValue, set} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {                                                                                              
  apiKey: "AIzaSyCqoaa-yaZrmIxs8Jtj2YxX1gzxqVi-J2c",
  authDomain: "proyectotawata.firebaseapp.com",
  dataBbaseURL:"https://proyectotawata-default-rtdb.firebaseio.com/",
  projectId: "proyectotawata",
  storageBucket: "proyectotawata.appspot.com",
  messagingSenderId: "93948459484",
  appId: "1:93948459484:web:ab8a31ba743b90fd167719",
  measurementId: "G-LEGJFBCKX9"
};

//inicializo firebase 
const app = initializeApp(firebaseConfig);
const auth = getAuth();
const database = getDatabase(app);



console.log("Bienvenido a la consola de pruebas.");
// Serial.println("...");

let sensadoREF = document.getElementById("volts");

var registroRef = document.getElementById("registroid");
//console.log(registroRef);

var ingresoRef = document.getElementById("ingresoid");
//console.log(ingresoRef);

var logoutRef = document.getElementById("logoutid");
//console.log(logoutRef);

var EncenderRef = document.getElementById("EncenderId")

var ApagarRef = document.getElementById("ApagarId")

var cardRef = document.getElementById("cardinicioid");
//console.log(cardRef);



var UserID;
var UserUID;
var usuario;



logoutRef.addEventListener("click", LogOut);



onAuthStateChanged(auth, (user) => {

    // solo se puede ver los botones para registrar e ingresar
    logoutRef.style.display = "none";
    ingresoRef.style.display = "block";
    registroRef.style.display = "block";
    sensadoREF.style.display = "none";
    ApagarRef.style.display = "none";
    EncenderRef.style.display = "none";
    
    
  
    if (user) {
        usuario = user
        // una vez iniciado sesión, se puede ver el botón para cerrar sesión 
        logoutRef.style.display = "block";
        ingresoRef.style.display = "none";
        registroRef.style.display = "none";
        sensadoREF.style.display = "block";
        ApagarRef.style.display = "block";
        EncenderRef.style.display = "block";
         
        get(ref(database, "users/" + user.uid)).then((snap) => {
          usuario = snap.val();
          UserID = usuario.Name;
          UserUID = user.uid;
        })
    }
});


function LogOut(){
  auth.signOut();
  location.reload(true);
}




const rutaRef = ref(database, 'dato/');
onValue(rutaRef,(snapshot) => {
  const data = snapshot.val();
  sensadoREF.innerHTML = data;
  
  
})

EncenderRef.addEventListener("click",encenderLaser);

const valor1 = 1;

function encenderLaser(){
  const db = getDatabase();
  set(ref(db, "laser1" ), valor1).then(() => {
    
})}

ApagarRef.addEventListener("click",apagarlaser);

const valor0 = 0;

function apagarlaser(){
  const db = getDatabase();
  set(ref(db, "laser1" ), valor0).then(() => {
    
})}


