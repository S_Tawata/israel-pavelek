// Import the functions you need from the SDKs you need
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";


// Your web app's Firebase configuration
const firebaseConfig = {                                                                                              
    apiKey: "AIzaSyCqoaa-yaZrmIxs8Jtj2YxX1gzxqVi-J2c",
    authDomain: "proyectotawata.firebaseapp.com",
    dataBbaseURL:"https://proyectotawata-default-rtdb.firebaseio.com/",
    projectId: "proyectotawata",
    storageBucket: "proyectotawata.appspot.com",
    messagingSenderId: "93948459484",
    appId: "1:93948459484:web:ab8a31ba743b90fd167719",
    measurementId: "G-LEGJFBCKX9"
  };
//inicializo firebase 
const app = initializeApp(firebaseConfig);

const auth = getAuth();

console.log("Bienvenido a la consola de pruebas.");
// Serial.println("...");

var lcorreoRef = document.getElementById("lcorreoId");
//console.log(lcorreoRef);

var lpasswordRef = document.getElementById("lpasswordId");
//console.log(lpasswordRef);

var ingresarRef = document.getElementById("ingresarbotonId");

//llamo a las funciones con un evento
ingresarRef.addEventListener("click", logIn);

// Creo la funcion logIn
function logIn ()
{

    if((lcorreoRef.value != '') && (lpasswordRef.value != '')){

        signInWithEmailAndPassword(auth, lcorreoRef.value, lpasswordRef.value) 
        .then((userCredential) => {
            const user = userCredential.user;
             // si esta todo correcto se direcciona al usuario a la página de inicio
            window.location.href = "../index.html";
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            alert("Revise el usuario o la contraseña")
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos esten completos.");
    }    
}



